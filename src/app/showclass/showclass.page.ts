import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-showclass',
  templateUrl: './showclass.page.html',
  styleUrls: ['./showclass.page.scss'],
})
export class ShowclassPage implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  image: any;
  result: any;
  feature: any;

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params && params.special && params.result && params.feature) {
        this.image = JSON.parse(params.special);
        this.result = JSON.parse(params.result);
        this.feature = JSON.parse(params.feature);
      }

      switch(this.feature.value){
        case "TEXT_DETECTION": {
          this.result = this.result.response[0].textAnnotations;
          break;
        }
      }

      console.log(this.result)
    });
  }

}
