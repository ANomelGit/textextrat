import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController, LoadingController } from '@ionic/angular';
import { GoogleCloudVisionServiceService } from '../google-cloud-vision-service.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  selectedfeature: "TEXT_DETECTION";

  constructor(
    private camera: Camera,
    private vision: GoogleCloudVisionServiceService,
    private route: Router,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) {}

  radioGroupChange(event){
    this.selectedfeature = event.detail;
  }

  async selectPhoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }

    this.camera.getPicture(options).then(async (ImageData) => {
      const loading = await this.loadingController.create({
        message: 'Obtention d\'image...',
        translucent : true
      });

      await loading.present();

      this.vision.getLabels(ImageData, this.selectedfeature).then(async (result) => {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            special: JSON.stringify(ImageData),
            result: JSON.stringify(result.data),
            feature: JSON.stringify(this.selectedfeature)
          }};

          this.route.navigate(["showclass"], navigationExtras);

          await loading.dismiss();

      }).catch( err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  }

  async presentAlertConfirm(){
    const alert = await this.alertController.create({
      header: 'Sélectionnez une option',
      message: 'Prenez une photo ou sélectionnez dans la galerie !!!',

      buttons: [
        {
          text: 'Camera',
          role: 'camera',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Galerie',
          role: 'gallary',
          handler: () => {
            this.takePhoto();
          }
        }
      ]

    });

    await alert.present();
  }

  async takePhoto(){
    const options: CameraOptions = {
      quality: 100,
      targetHeight: 500,
      targetWidth: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then(async (ImageData) => {
      const loading = await this.loadingController.create({
        message: 'Obtention d\'image...',
        translucent : true
      });

      await loading.present();

      this.vision.getLabels(ImageData, this.selectedfeature).then(async (result) => {
        console.log(result.data);
        let navigationExtras: NavigationExtras = {
          queryParams: {
            special: JSON.stringify(ImageData),
            result: JSON.stringify(result.data),
            feature: JSON.stringify(this.selectedfeature)
          }};

          this.route.navigate(["showclass"],navigationExtras)
          await loading.dismiss()
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  }
}

